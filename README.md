# What is this?

A skeleton [PresideCMS](https://www.preside.org) application shipped with [Bootstrap 5.3.3](https://getbootstrap.com/) and corresponding [Bootstrap Icons 1.11.3](https://icons.getbootstrap.com/) to be used as a pretty cool starting point for most responsive mobile-first web applications.

# Installation

This package should be installed using the `preside new site` command from CommandBox rather than directly. See [PresideCMS Commands](https://www.forgebox.io/view/preside-commands). Also you can run the `preside new site preside-skeleton-bootstrap5` directly.

# Details

## used Libraries

Living on the edge! This skeleton is using the following libraries:

    - Bootstrap Version 5.3.3 via [CDN](https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js)
    - Bootstrap Icons Version 1.11.3 via [CDN](https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css)

## Taskrunner

Feel free to use the included main.less file and the gulp taskrunner.
Compile the less-file with `lessc main.less` to the directory `/assets/css/src/`.
Run the gulpfile by typing the `gulp assemble` command. Make sure to have installed the following modules:

    - gulp-clean-css
    - gulp-rename
    - gulp-hash-filename
    - gulp-clean
