<!--- 
	This layout-template is meant to be a showcase for the bootstrap classes and icons with static navigation and footer.
	Please implement your own layout or just rename the index.template to index.cfm 
--->

<cfscript>
	body     = renderView();
	mainNav  = renderViewlet( "core.navigation.mainNavigation" );
	footer	 = renderViewlet( "core.footer.mainFooter" );
	metaTags = renderView( "/general/_pageMetaForHtmlHead" );
	
	
	event.include( "css-bs" )
	.include( "css-bs-icons" )
	.include( "css-main" )
	.include( "js-bs"  );
</cfscript>

<cfoutput><!DOCTYPE html>
	<html>
		<head>
			#metaTags#
			
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			
			#event.renderIncludes( "css" )#
			
			<!--[if lt IE 9]>
				<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
				<![endif]-->
			</head>
			<body>
				
				<style>
					.bd-placeholder-img {
						font-size: 1.125rem;
						text-anchor: middle;
						-webkit-user-select: none;
						-moz-user-select: none;
						user-select: none;
					}
					
					.themed-grid-col {
						padding-top: .75rem;
						padding-bottom: .75rem;
						background-color: rgba(86, 61, 124, .15);
						border: 1px solid rgba(86, 61, 124, .2);
					}
					
					.themed-container {
						padding: .75rem;
						margin-bottom: 1.5rem;
						background-color: rgba(0, 123, 255, .15);
						border: 1px solid rgba(0, 123, 255, .2);
					}			
					
					@media (min-width: 768px) {
						.bd-placeholder-img-lg {
							font-size: 3.5rem;
						}
					}
					
				</style>	
				
				<div class="container-fluid my-4" id="mainNavigation">
					#mainNav#
				</div>
				
				<div class="container-fluid" id="mainContent">
					#body#
				</div>

				<div class="container-fluid" id="mainNavigation">
					#footer#
				</div>

				#event.renderIncludes( "js" )#
			</body>
		</html></cfoutput>