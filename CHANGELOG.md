## 18.2.0 (October 26, 2023)

### Bootstrap
* updated to version 5.3.2

### Bootstrap Icons
* updated to version 1.11.1
* switched to minified version

### general
* cleanup
* gitignore node_modules
* bugfix stickerbundle