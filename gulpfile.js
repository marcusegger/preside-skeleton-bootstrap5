// Install all necessary gulp modules using (depends on Node & Gulp):
// npm install gulp gulp-clean-css gulp-hash-filename gulp-rename gulp-clean gulp-sourcemaps gulp-less

const gulp = require("gulp");
const cleanCSS = require("gulp-clean-css");
const rename = require("gulp-rename");
const hash = require("gulp-hash-filename");
const clean = require("gulp-clean");
const sourcemaps = require("gulp-sourcemaps"); // inline source maps are embedded in the source file
const less = require("gulp-less"); // converts less into css

//clean up old CSS files
gulp.task("cleanCSS", function () {
  return gulp
    .src(["./assets/css/dist/*.css", "./assets/css/dist/*.css.map"], {
      read: false,
    })
    .pipe(clean());
});

//prepare stylesheets for production
gulp.task("assembleCSS", () => {
  return gulp
    .src(["./assets/css/src/*.css", "!./assets/css/src/*.min.css"])
    .pipe(sourcemaps.init())
    .pipe(cleanCSS())
    .pipe(hash({ format: "{name}-{hash:8}.min{ext}" }))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./assets/css/dist"));
});

// Generate CSS files from LESS
gulp.task("less", function () {
  return gulp
    .src(["./assets/css/less/**/*.less", "!./assets/css/less/globals.less"])
    .pipe(less())
    .pipe(gulp.dest("./assets/css/src"));
});

//run all tasks for final assets
gulp.task("assemble", gulp.series("less", "cleanCSS", "assembleCSS"), function () {});
