/**
 * Sticker bundle configuration file. See: http://sticker.readthedocs.org/
 */
component {

	public void function configure( bundle ) {
		
		bundle.addAsset( id="js-bs", url="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" );
		bundle.addAsset( id="css-bs", url="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" );
		bundle.addAsset( id="css-bs-icons", url="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css" );
		bundle.addAsset( id="css-main", path="/css/dist/main*.min.css" );

		bundle.asset( "css-bs" ).before( "*" );
	}

}